ezstream
========

You want to stream media from the web, but using a proper player instead of silly web interfaces?

ezstream uses youtube-dl to extract the media URL from any supported link.
The media URL will then be opened by any desired player.

Usage
-----

    ezstream <url> [<player>]
        url:    URL that is supported by youtube-dl
        player: A player application (default is vlc)

Examples
--------

Play some video from youtube:

    ezstream https://www.youtube.com/watch?v=Cy4mztkndHk

Play the same video, but using `mpv` instead of `vlc`:

    https://www.youtube.com/watch?v=Cy4mztkndHk mpv

Audio-only also works:

    https://www.mixcloud.com/pat-styx/dub-cast-012/

On supported sites, you can even play playlists:

    ezstream https://www.mixcloud.com/vidister/
    ezstream https://www.youtube.com/playlist?list=PL_1fUsHSTFMBJMASIsgycNsr44AfYx7uh

Installation
------------

You can just copy the file `ezstream` to `~/bin`:

    git pull https://gitlab.com/ahiiti/ezstream.git
    cp ezstream/ezstream ~/bin/ezstream

Alternatively, you could also create a symlink between this repository and `~/bin`:

    git pull https://gitlab.com/ahiiti/ezstream.git
    ln -sr ezstream/ezstream ~/bin/ezstream

This way, you can update ezstream by running `git pull`, without copying it again.

